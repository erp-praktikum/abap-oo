# Chapter 9: Events - Raising and Handling Events

When you have finished this exercise, you are able to:

- Raise and handle events

Solution: [Link](solution/solution.md)

1. The class `lcl_airplane` shall declare the event `airplane_created`, and raise it whenever an instance is created.

2. The class `lcl_carrier` shall handle these events.

    - Define and implement an event handler method `on_airplane_created`.
      It should add every newly created airplane to the list of airplanes.
    - Activate the event handling in the constructor (`SET HANDLER`)

3. In the main method, remove all calls to `add_airplane`.

    - Are all planes including cargo and passenger planes still shown in the carrier details?
    - What could we do if we only want to add passenger planes automatically?

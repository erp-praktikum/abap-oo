# (Optional) Chapter 12:  Advanced Techniques - Friend Relationships

When you have finished this exercise, you are able to:

- Create a friend relationship between classes and exchange data between the related classes.

Note: The `friend` concept is mostly only used in large projects with a complex class structure
which is thoroughly planned and where other solutions have been deeply discussed.

Solution: [Link](solution/solution.md)

1. Use the singleton and the main program you used in the previous exercise.

    - make the attribute `shared_text` in the singleton class private
    - Add a public method `get_shared_text` that returns the value of the now private attribute `shared_text`.

2. Create a class `Z_##_FRIEND` that can access the private attribute using a friend relationship.

    - the class shall have a public method `set_shared_text`
    - implement the method so that it gets an instance of the singleton class,
      and set its `shared_text` attribute to a value given by importing parameter.
      This should result in an error, because `shared_text` is private.

3. Switch to your singleton class, add `Z_##_FRIEND` as a friend of your singleton.

4. In the singleton main class `Z_##_SINGLETON_MAIN`:

    - Check what happens when you try to execute the previous code
    - Replace the read access to `shared_text` with calls to the method `get_shared_text`
    - Create an object of type `Z_##_FRIEND` within your main program
    - Replace the write access to `shared_text` with a call to the method `set_shared_text`

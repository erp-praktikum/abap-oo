# main class

This class does not use local types.

    CLASS z_abapoo_main_15 DEFINITION
      PUBLIC
      FINAL
      CREATE PUBLIC .
    
      PUBLIC SECTION.
    
        INTERFACES if_oo_adt_classrun .
      PROTECTED SECTION.
      PRIVATE SECTION.
    ENDCLASS.
    
    
    
    CLASS z_abapoo_main_15 IMPLEMENTATION.
    
    
      METHOD if_oo_adt_classrun~main.
        DATA: l_private_singleton1 TYPE REF TO z_abapoo_private_singleton,
              l_private_singleton2 TYPE REF TO z_abapoo_private_singleton,
              l_friend             TYPE REF TO z_abapoo_friend.
    
        l_private_singleton1 = z_abapoo_private_singleton=>get_instance( ).
        l_private_singleton2 = z_abapoo_private_singleton=>get_instance( ).
    
        out->write( l_private_singleton1->get_shared_text( ) ).
        out->write( l_private_singleton2->get_shared_text( ) ).
    
        l_friend = NEW #(  ).
        l_friend->set_shared_text( 'new text' ).
    
        out->write( l_private_singleton1->get_shared_text( ) ).
        out->write( l_private_singleton2->get_shared_text( ) ).
    
      ENDMETHOD.
    ENDCLASS.

# singleton class

This class does not use local types.

    CLASS z_abapoo_private_singleton DEFINITION
      PUBLIC
      FINAL
      CREATE PRIVATE
      GLOBAL FRIENDS z_abapoo_friend.
    
      PUBLIC SECTION.
    
        METHODS get_shared_text RETURNING VALUE(re_shared_text) TYPE string.
    
        CLASS-METHODS class_constructor.
    
        CLASS-METHODS get_instance
          RETURNING VALUE(re_instance) TYPE REF TO z_abapoo_private_singleton.
    
      PRIVATE SECTION.
        DATA shared_text  TYPE string VALUE 'initial value'.
        CLASS-DATA instance TYPE REF TO z_abapoo_private_singleton.
    
    ENDCLASS.
    
    
    
    CLASS z_abapoo_private_singleton IMPLEMENTATION.
    
    
      METHOD class_constructor.
        z_abapoo_private_singleton=>instance = NEW #(  ).
      ENDMETHOD.
    
    
      METHOD get_instance.
        re_instance = z_abapoo_private_singleton=>instance.
      ENDMETHOD.
    
      METHOD get_shared_text.
        re_shared_text = me->shared_text.
      ENDMETHOD.
    
    ENDCLASS.

# friend class

This class does not use local types.

    CLASS z_abapoo_friend DEFINITION
      PUBLIC
      FINAL
      CREATE PUBLIC .
    
      PUBLIC SECTION.
        METHODS set_shared_text IMPORTING im_new_shared_text TYPE string.
      PROTECTED SECTION.
      PRIVATE SECTION.
    ENDCLASS.
    
    
    
    CLASS z_abapoo_friend IMPLEMENTATION.
      METHOD set_shared_text.
        DATA(l_singleton) = z_abapoo_private_singleton=>get_instance(  ).
        l_singleton->shared_text = im_new_shared_text.
      ENDMETHOD.
    
    ENDCLASS.

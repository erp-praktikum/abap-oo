# Chapter 10: Global Classes/Interfaces - Recap

When you have finished this exercise, you are able to:

- Create and edit global classes and interfaces

Solution: [Link](solution/solution.md) 

A travel agency communicates with its business partners. Those partners shall be the contracted hotels.

1. Create the global class `Z_##_HOTEL`. The class shall contain these attributes and methods:

    - `name` type `string` as a private instance attribute
    - `max_beds` type `i` as a private instance attribute
    - `constructor` importing values for `name` and `max_beds`.
    - `display_attributes` for displaying instance attributes
    - the class does **not** use a main method, and therefor does **not** implement the interface `if_oo_adt_classrun`

3. Implement the methods and activate your class.

4. Define the global interface `Z_##_PARTNER` containing the method `display_partner`.

5. Let `Z_##_HOTEL` implement this interface.

6. Switch back to your main class and replace local interface `lif_partner` with global interface `Z_##_PARTNER`.

7. Create some hotels and add them to the business partners of your travel agency.

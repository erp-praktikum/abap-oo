# Chapter 4: Basics - Calling Methods

When you have finished this exercise, you are able to:

- Call instance and static methods

Solution: [Link](solution/solution.md)

1. Implement the display* methods. The importing parameter (`im_out`) is a reference to an object with a method write,
it can be used like `out->write()` in the main program .

2. In your main method, call the method `display_number_of_airplanes` before you create an object of `lcl_airplane`.

3. Set the attributes for every already created object using the method `set_attributes`.
   Choose a name and a plane type and hand them over to the method using text literals.
   Is there a restriction what values can be used for plane type?

4. Display the attributes for every object using the `display_attributes` method.

5. After creating all airplane objects, call the method `display_number_of_airplanes` again.

6. Expand the class `lcl_airplane` by the static method `get_number_of_airplanes`.

    - This method shall have a returning parameter `re_number_of_airplanes` of suitable type and has no input parameter.
    - It shall deliver the current value of static member `number_of_airplanes`.
    - Test your method by calling it from the main program and print the result to the output.
    - What is the difference to using `display_number_of_airplanes`? Can you think of advantages/disadvantages?

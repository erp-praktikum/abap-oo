# Chapter 4: Basics - Constructor

When you have finished this exercised, you are able to:

- Add a constructor to a class
- Instantiate an object using a constructor

Solution: [Link](solution/solution.md)

1. Create a constructor for your class `lcl_airplane`.
   The easiest way to do so is to refactor the method `set_attributes` (which will not be used anymore) into a constructor.

    - the constructor shall have two importing parameters, for the respective attributes `name` and `planetype` within the class.
    - within the constructor the attribute `number_of_airplanes` shall be incremented by one.

3. In case you did not rename `set_attributes` to your constructur:
   Put the line of code, which is incrementing the attribute `number_of_airplanes` in method `set_attributes`, into a comment,
   so `number_of_airplanes` will not be incremented in this method anymore (as we now handle this via constructor).

4. In the main program, extend the creation of an object by providing the parameters that are mandatory by the
   definition of the constructor. Provide the same attributes used for `set_attributes` now to the constructor.

5. Delete the call of the method `set_attributes`, or put it into comments.

# Global Class

    CLASS z_abapoo_main_05 DEFINITION
      PUBLIC
      FINAL
      CREATE PUBLIC .
    
      PUBLIC SECTION.
        INTERFACES if_oo_adt_classrun.
      PROTECTED SECTION.
      PRIVATE SECTION.
    ENDCLASS.
    
    
    
    CLASS Z_ABAPOO_MAIN_05 IMPLEMENTATION.
    
    
      METHOD if_oo_adt_classrun~main.
        DATA l_airplane TYPE REF TO lcl_airplane.
        DATA l_airplanes TYPE TABLE OF REF TO lcl_airplane.
    
        lcl_airplane=>display_number_of_airplanes( out ).
    
        l_airplane = NEW lcl_airplane( im_name = 'Plane1' im_planetype = 'A700' ).
        l_airplane->display_attributes( out ).
        APPEND l_airplane TO l_airplanes.
    
        l_airplane = NEW lcl_airplane( im_name = 'Plane2' im_planetype = 'A600' ).
        l_airplane->display_attributes( out ).
        APPEND l_airplane TO l_airplanes.
    
        l_airplane = NEW lcl_airplane( im_name = 'Plane3' im_planetype = 'Boeing' ).
        l_airplane->display_attributes( out ).
        APPEND l_airplane TO l_airplanes.
    
        l_airplane = NEW lcl_airplane( im_name = 'Plane4' im_planetype = 'Airbus' ).
        l_airplane->display_attributes( out ).
        APPEND l_airplane TO l_airplanes.
    
        lcl_airplane=>display_number_of_airplanes( out ).
    
    * Another way to show the data:
        DATA(l_number_of_airlines) = lcl_airplane=>get_number_of_airplanes( ).
        out->write( |Number of airplanes alternative: { l_number_of_airlines }| ).
    
      ENDMETHOD.
    ENDCLASS.

# Local Types

    *"* use this source file for the definition and implementation of
    *"* local helper classes, interface definitions and type
    *"* declarations
    CLASS lcl_airplane DEFINITION.
    
      PUBLIC SECTION.
        METHODS constructor
          IMPORTING
            im_name      TYPE string
            im_planetype TYPE /dmo/plane_type_id.
    
        METHODS display_attributes
          IMPORTING im_out TYPE REF TO if_oo_adt_classrun_out.
    
        CLASS-METHODS display_number_of_airplanes
          IMPORTING im_out TYPE REF TO if_oo_adt_classrun_out.
    
        CLASS-METHODS get_number_of_airplanes
          RETURNING VALUE(re_number_of_airplanes) TYPE i.
    
      PROTECTED SECTION.
    
      PRIVATE SECTION.
        DATA name TYPE string.
        DATA planetype TYPE /dmo/plane_type_id.
    
        CLASS-DATA number_of_airplanes TYPE i.
    
    ENDCLASS.
    
    CLASS lcl_airplane IMPLEMENTATION.
      METHOD constructor.
        name = im_name.
        planetype = im_planetype.
        lcl_airplane=>number_of_airplanes = lcl_airplane=>number_of_airplanes + 1.
      ENDMETHOD.
    
      METHOD display_attributes.
        im_out->write( |{ name }: { planetype }| ).
      ENDMETHOD.
    
      METHOD display_number_of_airplanes.
        im_out->write( |Number of airplanes: { lcl_airplane=>number_of_airplanes }| ).
      ENDMETHOD.
    
      METHOD get_number_of_airplanes.
        re_number_of_airplanes = lcl_airplane=>number_of_airplanes.
      ENDMETHOD.
    
    ENDCLASS.

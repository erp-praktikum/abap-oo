# Chapter 8: Interfaces - Defining Interfaces

When you have finished this exercise, you are able to:

- Define and implement interfaces

Solution: [Link](solution/solution.md)

The classes `lcl_rental` and `lcl_hotel` were added to the UML diagram below.
During the exercise the classes `lcl_rental`, `lcl_carrier` and later `lcl_hotel` shall outsource common services
and provide an interface for users, which will be called `lif_partner`.

1. Add the missing classes and the interface `lif_partner` to your UML diagram.
   The only method that shall be provided in the interface is `display_partner`.
   Think about what role `lcl_carrier`, `lcl_rental` and `lcl_hotel` have to fulfill.

   ![uml template](10_UML.png)

2. Copy the interface `lif_partner` and the class `lcl_rental` to the local types of your class.

        INTERFACE lif_partner.
          METHODS display_partner
            IMPORTING im_out TYPE REF TO if_oo_adt_classrun_out.
        ENDINTERFACE.
        
        CLASS lcl_rental DEFINITION.
          PUBLIC SECTION.
            METHODS constructor
              IMPORTING
                im_name TYPE string.
        
            INTERFACES lif_partner.
        
          PROTECTED SECTION.
            DATA name TYPE string.
        ENDCLASS.
        
        CLASS lcl_rental IMPLEMENTATION.
          METHOD lif_partner~display_partner.
            im_out->write( 'Displaying rental.' && me->name ).
          ENDMETHOD.
          METHOD constructor.
            me->name = im_name.
          ENDMETHOD.
        
        ENDCLASS.

3. Make class `lcl_carrier` implement interface `lif_partner`.

4. Create some objects of `lcl_rental` and `lcl_carrier` in your main program.

(Usage of the interface is part of the next exercise.)


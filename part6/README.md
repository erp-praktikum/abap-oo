# Chapter 4: Basics - Calling Private Methods

When you have finished this exercise, you are able to:

- Call a method within a class

Solution: [Link](solution/solution.md);
The DDIC objects are not shown in the solution.

###	Recap: Create DDIC objects and data

Create DDIC objects:

1. Domain `Z_##_AIRPLANE_WEIGHT`, type `int4`
2. Data element `Z_##_AIRPLANE_WEIGHT`, based on that domain
3. Database table `Z##_AIRPLANE` with fields:

    - `client`, type `abap.clnt`
    - `type_id` type `/dmo/plane_type_id`
    - `weight` type `Z_##_AIRPLANE_WEIGHT`

### Private method calls

Create the private instance method `get_technical_attributes` within class `lcl_airplane`.

1. This method shall import a value by a parameter typed with the plane type id data element.
2. The method shall export the planes weight in parameter ex_weight.
3. The export parameter shall be filled with the value read from the database table created in the step above, queried on the given plane type.
4. If there is no record in the table for the given plane type, then the default value weight: 100.000 shall be returned.
Without the optional step (data generator) below, the select will never find data, and the default value will always be returned, that's ok.
Otherwise you should see the values you have stored in the database table for a corresponding planetype.
5. Test your method `get_technical_attributes`

    - by calling it from the main program;
    - by calling it within the method display_attributes.
    - Which of the two ways was permitted/successful and why?

### Optional: Data generator 

Create a data generator class, and fill the table `Z##_AIRPLANE` with some values.
For `type_id` you should use the same values you are using in your main program, when instantiating airplane objects.
You can check database table `/DMO/FLIGHT` for values SAP uses in the demo data for `plane_type_id`s, but it is not necessary to use those.

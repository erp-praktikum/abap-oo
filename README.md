# Exercises ABAP Objects

Most exercises build onto the previous parts.
- You can continuously work in one class, and add the changes to a single main class.
- You can also duplicate your work after each exercise, and do the changes for the next exercise in the new copy.
  The solutions use this approach, adding numbers at the end of the class name (e.g. `Z_ABAPOO_MAIN_03`) 
- If you need a clean state, you can also copy the solution of the previous exercise, and continue from that.

## Chapters

### [Chapter 3: Analysis and Design - UML](part1/README.md)

### [Chapter 4: Basics - Creating Classes](part2/README.md)

### [Chapter 4: Basics - Instantiating Objects](part3/README.md)

### [Chapter 4: Basics - Calling Methods](part4/README.md)

### [Chapter 4: Basics - Constructor](part5/README.md)

### [Chapter 4: Basics - Calling Private Methods](part6/README.md)

### [Chapter 5: Inheritance - Creating Class Hierarchies](part7/README.md)

### [Chapter 6: Casting - Polymorphy](part8/README.md)

### [Chapter 7: Exceptions - Defining, Triggering, Propagating and Catching](part9/README.md)

### [Chapter 8: Interfaces - Defining Interfaces](part10/README.md)

### [Chapter 8: Interfaces - Implementing Interfaces](part11/README.md)

### [Chapter 9: Events - Raising and Handling Events](part12/README.md)

### [Chapter 10: Global Classes/Interfaces - Recap](part13/README.md)

### [Chapter 11: Advanced Techniques - Singleton Classes](part14/README.md)

### [(Optional) Chapter 12:  Advanced Techniques - Friend Relationships](part15/README.md)

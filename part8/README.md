# Chapter 6: Casting - Polymorphy

When you have finished this exercise, you are able to:

- Describe polymorphy
- Use generic programming

Solution: [Link](solution/solution.md)<br>
The solution has two parts, the first part contains the solution after implementing the first step.
You can use it to debug and answer the last questions of step 1. 
The second part is the full solution after implementing all steps.

1. Expand your existing main method:

    - If it does not already exist, create an internal table for buffering airplane objects.
      The tables line type shall be `REF TO lcl_airplane`.
    - Try inserting the already existing airplane objects (airplanes, passenger planes and cargo planes) into the internal table.
    - The table shall be scanned within a loop using an auxiliary variable of appropriate type.
    - Display the attributes of every airplane using `display_attributes` within the `LOOP` statement.
    - Check the internal table and the execution of the method `display_attributes` using the debugger.
    - Which code is executed by calling `display_attributes` – the original method from the super class or the redefined method from the sub classes?
    - What would happen if one of these methods would not be redefined within one of the sub classes?

2. Define and implement class `lcl_carrier`, which shall encapsulate a list of airplanes that are property of an airline.

    - Add a private instance attribute `airplanes` typified as a table of appropriate line type to serve as an airplane list.
    - Add a private instance attribute `name` of type string and force it being set by demanding it from the constructor.

3. Add the following public instance methods to lcl_carrier:

    - A method `add_airplane`, which adds airplanes to the airplane list within the `lcl_carrier` object.
      The given parameter is a reference to the `lcl_airplane` class.
    - A method `display_airplanes`, which displays the airplanes from the airplane_list using the method `display_attributes` from the `lcl_airplane` class.
    - A method `display_attributes`, which displays the carrier’s attributes. Attributes are their name and the planes they own.

4. Switch to the main program

    - Create a reference to the `lcl_carrier` class, using the `DATA` statement.
    - Remove all statements regarding the main programs internal table.
    - Create an airline object `TYPE REF TO lcl_carrier` and provide values for the constructor
    - Append the created planes to the airlines plane list using the `add_airplane` method of the `lcl_carrier class`.
    - Create more airplane objects and append them to the list.
    - Display the airlines attributes, using the `display_attributes` method of `lcl_carrier`
    - The `display_attributes` method of `lcl_carrier` calls `display_attributes` for every entry in the list of airplanes.
      Which code is executed? 

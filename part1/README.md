# Chapter 3: Analysis and Design - UML

When you have finished this exercise, you are able to:

- Create a UML class diagram

Solution: [UML](solution/carrier_class_diagram.png)

An airline wants to organize their planes.

1. Take a sheet of paper and a pen and start drawing a class diagram using UML notation,
   containing the following classes:

    - Airline: lcl_carrier
    - Plane (general): lcl_airplane
    - Passenger plane: lcl_passenger_plane
    - Cargo plane: lcl_cargo_plane

2. Think about useful attributes and methods for each class and add them to the diagram.

3. Draw the relationships between classes and specify possible cardinalities.
